
Test task for creating a multitimer

---

# Multitimer


#### The user creates a timer by clicking on the "+" button on the top panel.
#### In the window that appears, the user specifies an arbitrary timer name and sets the time interval.
#### Each new timer is added to the table. The timer has a countdown.  After the time has elapsed, the timer is removed from the table.

---
![](https://bitbucket.org/Listopadov/timers/downloads/Simulator_Screen_Shot_-_iPhone_13_Pro_Max_-_2022-04-11_at_15.38.51.png)

---
